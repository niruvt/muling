--------------------------------------------------------------------------
Class:        muling
Author:       Niranjan
Version:      0.2  (13 November, 2020)
Description:  A class file for the Department of Linguistics, University of
              Mumbai
Repository:   https://gitlab.com/niranjanvikastambe/muling
Bug tracker:  https://gitlab.com/niranjanvikastambe/muling/-/issues
License:      The LaTeX Project Public License v1.3c or later.
--------------------------------------------------------------------------
